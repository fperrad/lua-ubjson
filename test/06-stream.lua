#! /usr/bin/lua

require'Test.Assertion'

plan(9)

local u = require'ubjson'

local t = {1, {2, 3}, {4, 5}}
same(u.decode(u.encode(t)), t, "array")

local s
s = u.open_array()
 .. u.encode(t[1])
 .. u.encode(t[2])
 .. u.open_array() .. u.encode(t[3][1]) .. u.encode(t[3][2]) .. u.close_array()
 .. u.close_array()
same(u.decode(s), t, "indefinite-length array")

s = u.open_array(3)
 .. u.encode(t[1])
 .. u.encode(t[2])
 .. u.open_array(2) .. u.encode(t[3][1]) .. u.encode(t[3][2])
same(u.decode(s), t, "array with size")

s = u.open_array(3)
 .. u.encode(t[1])
 .. u.encode(t[2])
 .. u.open_array(2, 'i') .. string.char(t[3][1], t[3][2])
same(u.decode(s), t, "array with size & type")

t = {Fed=71, Abc=65}
same(u.decode(u.encode(t)), t, "object")

s = u.open_object()
 .. u.encode_name('Fed') .. u.encode(71)
 .. u.encode_name('Abc') .. u.encode(65)
 .. u.close_object()
same(u.decode(s), t, "indefinite-length object")

s = u.open_object(2)
 .. u.encode_name('Fed') .. u.encode(71)
 .. u.encode_name('Abc') .. u.encode(65)
same(u.decode(s), t, "object with size")

s = u.open_object(2, 'i')
 .. u.encode_name('Fed') .. string.char(71)
 .. u.encode_name('Abc') .. string.char(65)
same(u.decode(s), t, "object with size & type")

equals(u.no_op(), 'N', "no-op")
