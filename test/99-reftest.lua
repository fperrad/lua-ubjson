#! /usr/bin/lua

require'Test.Assertion'

plan(3)

local u = require'ubjson'
local j = require'dkjson'

local function slurp (filename)
    local f, msg =  io.open(filename, 'r')
    if not f then
        return nil, msg
    end
    local content = f:read('*a')
    f:close()
    return content
end

--[[
    https://github.com/thebuzzmedia/universal-binary-json-java/tree/master/src/test/resources/org/ubjson
    supplies json files but unfortunately, associated ubj files follow the Draft8 specifications.

    ubj files have been regenerated from formatted.json by
    https://dmitry-ra.github.io/ubjson-test-suite/json-converter.html
]]

same( u.decode(assert(slurp('test/resources/MediaContent.ubj'))),
      j.decode(assert(slurp('test/resources/MediaContent.compact.json'))),
      "MediaContent" )

same( u.decode(assert(slurp('test/resources/TwitterTimeline.ubj'))),
      j.decode(assert(slurp('test/resources/TwitterTimeline.compact.json'))),
      "TwitterTimeline" )

if #string.pack('n', 0.0) > 8 then
    skip("long double", 1)
else
    same( u.decode(assert(slurp('test/resources/CouchDB4k.ubj'))),
          j.decode(assert(slurp('test/resources/CouchDB4k.compact.json'))),
          "CouchDB4k" )
end
