#! /usr/bin/lua

require'Test.Assertion'

plan(28)

local u = require'ubjson'

error_matches( function ()
                u.set_string'bad'
        end,
        "bad argument #1 to set_string %(invalid option 'bad'%)" )

error_matches( function ()
                u.set_array'bad'
        end,
        "bad argument #1 to set_array %(invalid option 'bad'%)" )

error_matches( function ()
                u.set_float'bad'
        end,
        "bad argument #1 to set_float %(invalid option 'bad'%)" )

error_matches( function ()
                u.open_array(2.5)
        end,
        "bad argument #1 to open_array %(positive integer expected, got number%)" )

error_matches( function ()
                u.open_array(42, 'z')
        end,
        "bad argument #2 to open_array %(UBJSON type expected, got z%)" )

error_matches( function ()
                u.open_object(2.5)
        end,
        "bad argument #1 to open_object %(positive integer expected, got number%)" )

error_matches( function ()
                u.open_object(42, 'z')
        end,
        "bad argument #2 to open_object %(UBJSON type expected, got z%)" )

error_matches( function ()
                u.encode( print )
        end,
        "encode 'function' is unimplemented" )

error_matches( function ()
                u.encode( coroutine.create(plan) )
        end,
        "encode 'thread' is unimplemented" )

error_matches( function ()
                u.encode( io.stdin )
        end,
        "encode 'userdata' is unimplemented" )

error_matches( function ()
                local a = {}
                a.foo = a
                u.encode( a )
        end,
        "stack overflow",   -- from Lua interpreter
        "direct cycle" )

error_matches( function ()
                local a = {}
                local b = {}
                a.foo = b
                b.foo = a
                u.encode( a )
        end,
        "stack overflow",   -- from Lua interpreter
        "indirect cycle" )

error_matches( function ()
                u.encode_name( 42 )
        end,
        "bad argument #1 to encode_key %(string expected, got number%)" )

error_matches( function ()
                u.decode( {} )
        end,
        "bad argument #1 to decode %(string expected, got table%)" )

error_matches( function ()
                u.decode(string.char(0x1C))
        end,
        "decode '0x1c' is unexpected" )

equals( u.decode(u.encode("text")), "text" )

error_matches( function ()
                u.decode(u.encode("text"):sub(1, -2))
        end,
        "missing bytes" )

error_matches( function ()
                u.decode(u.encode("text") .. "more")
        end,
        "extra bytes" )

error_matches( function ()
                u.decode(u.encode("text") .. "1")
        end,
        "extra bytes" )

error_matches( function ()
                u.decoder( false )
        end,
        "bad argument #1 to decoder %(string or function expected, got boolean%)" )

error_matches( function ()
                u.decoder( {} )
        end,
        "bad argument #1 to decoder %(string or function expected, got table%)" )

for _, val in u.decoder(string.rep(u.encode("text"), 2)) do
    equals( val, "text" )
end

error_matches( function ()
                for _, val in u.decoder(string.rep(u.encode("text"), 2):sub(1, -2)) do
                    equals( val, "text" )
                end
        end,
        "missing bytes" )

error_matches( function ()
                u.decode("SU\1\199")
        end,
        "invalid UTF%-8 string" )

u.strict = false
equals( u.decode("SU\1\199"), "\199", "invalid utf8" )

not_errors( function ()
                for _ in ipairs(u.coders) do end
        end,
        "cannot iterate packers" )
