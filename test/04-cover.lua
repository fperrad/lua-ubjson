#! /usr/bin/lua

require'Test.Assertion'

plan(14)

local u = require'ubjson'

equals( u.decode(u.encode(true)), true, "true" )
equals( u.decode(u.encode(false)), false, "false" )

u.set_float'float32'
local nan = u.decode(u.encode(0/0))
is_number( nan, "nan" )
truthy( nan ~= nan )
equals( u.decode(u.encode(3.140625)), 3.140625, "3.140625" )
equals( u.decode(u.encode(-2)), -2, "neg -2" )
equals( u.decode(u.encode(42)), 42, "pos 42" )

u.set_float'high-precision'
equals( u.encode(-0.5), "HU\4-0.5", "-0.5")

u.set_string'check_utf8'
equals( u.encode("abc\100"), "SU\4abc\100", "text" )
equals( u.encode("abc\200"), "[$U#U\4abc\200", "binary" )

equals( u.open_array(255), "[#U\255", "# 255" )
equals( u.open_array(4095), "[#I\15\255", "# 4095" )
equals( u.open_array(16777215), "[#l\0\255\255\255", "# 16777215" )
if #string.pack('n', 0.0) == 4 then
    skip( "small Lua", 1 )
else
    equals( u.open_array(68719476735), "[#L\0\0\0\15\255\255\255\255", "# 68719476735" )
end
