#! /usr/bin/lua

require 'Test.Assertion'

plan(18)

if not require_ok'ubjson' then
    BAIL_OUT"no lib"
end

local m = require'ubjson'
is_table( m )
matches( m._COPYRIGHT, 'Perrad', "_COPYRIGHT" )
matches( m._DESCRIPTION, 'ubjson', "_DESCRIPTION" )
matches( m._VERSION, '^%d%.%d%.%d$', "_VERSION" )

is_table( m.coders, "table coders" )
is_function( m.decode, "function decode" )
is_function( m.decode_cursor, "function decode_cursor" )
is_function( m.encode, "function encode" )
is_function( m.encode_name, "function encode_name" )
is_function( m.set_float, "function set_float" )
is_function( m.set_string, "function set_string" )
is_function( m.set_array, "function set_array" )
is_function( m.open_array, "function open_array" )
is_function( m.close_array, "function close_array" )
is_function( m.open_object, "function open_object" )
is_function( m.close_object, "function close_object" )
is_function( m.no_op, "function no_op" )

if _VERSION >= 'Lua 5.3' then
    diag "full 64bits with Lua >= 5.3"
end
