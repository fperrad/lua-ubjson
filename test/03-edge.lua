#! /usr/bin/lua

require'Test.Assertion'

plan(35)

local u = require'ubjson'

if #string.pack('n', 0.0) > 8 and _VERSION == 'Lua 5.3' then
    skip("long double", 4)
else
    equals( u.decode(u.encode(1.0/0.0)), 1.0/0.0, "inf" )
    equals( u.decode(u.encode(-1.0/0.0)), -1.0/0.0, "-inf" )

    local nan = u.decode(u.encode(0.0/0.0))
    is_number( nan, "nan" )
    truthy( nan ~= nan )
end

local t = setmetatable( { 'a', 'b', 'c' }, { __index = { [4] = 'd' } } )
equals( t[4], 'd' )
t = u.decode(u.encode(t))
equals( t[2], 'b' )
equals( t[4], nil, "don't follow metatable" )

t = setmetatable( { a = 1, b = 2, c = 3 }, { __index = { d = 4 } } )
equals( t.d, 4 )
t = u.decode(u.encode(t))
equals( t.b, 2 )
equals( t.d, nil, "don't follow metatable" )

t = { 10, 20, nil, 40 }
u.set_array'without_hole'
equals( u.encode(t):byte(2), u.open_array(2):byte(2), "array truncated by hole" )
same( u.decode(u.encode(t)), t )
u.set_array'with_hole'
equals( u.encode(t):byte(2), u.open_array(4):byte(2), "array with hole" )
same( u.decode(u.encode(t)), t )

t = {}
u.set_array'without_hole'
equals( u.encode(t):byte(), u.open_object():byte(), "empty table as object" )
u.set_array'with_hole'
equals( u.encode(t):byte(), u.open_array():byte(), "empty table as array" )
u.set_array'always_as_object'
equals( u.encode(t):byte(), u.open_object():byte(), "empty table as object" )

u.set_float'float32'
equals( u.encode(3.402824e+38), u.encode(1.0/0.0), "float 3.402824e+38")
equals( u.encode(7e42), u.encode(1.0/0.0), "inf (downcast double -> float)")
equals( u.encode(-7e42), u.encode(-1.0/0.0), "-inf (downcast double -> float)")
equals( u.decode(u.encode(7e42)), 1.0/0.0, "inf (downcast double -> float)")
equals( u.decode(u.encode(-7e42)), -1.0/0.0, "-inf (downcast double -> float)")
equals( u.decode(u.encode(7e-46)), 0.0, "epsilon (downcast double -> float)")
equals( u.decode(u.encode(-7e-46)), -0.0, "-epsilon (downcast double -> float)")

u.set_float'float64'
if #string.pack('n', 0.0) > 8 then
    equals( u.encode(7e400), u.encode(1.0/0.0), "inf (downcast long double -> double)")
    equals( u.encode(-7e400), u.encode(-1.0/0.0), "-inf (downcast long double -> double)")
    equals( u.decode(u.encode(7e400)), 1.0/0.0, "inf (downcast long double -> double)")
    equals( u.decode(u.encode(-7e400)), -1.0/0.0, "-inf (downcast long double -> double)")
    equals( u.decode(u.encode(7e-400)), 0.0, "epsilon (downcast long double -> double)")
    equals( u.decode(u.encode(-7e-400)), -0.0, "-epsilon (downcast long double -> double)")
else
    skip("no long double", 6)
end

if _VERSION >= 'Lua 5.3' then
    equals( u.decode(u.encode(0xFFFFFFFFFFFFFFFF)), 0xFFFFFFFFFFFFFFFF, "64 bits")
else
    skip("53 bits", 1)
end

local ubj = "{U\2idi\5U\2idi\7}"  -- { id = 5, id = 7 }
error_matches(function ()
               u.decode(ubj)
        end,
        "duplicated keys" )

u.strict = false
not_errors(function ()
             u.decode(ubj)
        end,
        "duplicated keys" )

u.strict = true
ubj = "{#U\2U\2idi\5U\2idi\7"  -- { id = 5, id = 7 }
error_matches(function ()
               u.decode(ubj)
        end,
        "duplicated keys" )

u.strict = false
not_errors(function ()
             u.decode(ubj)
        end,
        "duplicated keys" )
