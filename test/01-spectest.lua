#! /usr/bin/lua

local unpack = table.unpack or unpack

require 'Test.Assertion'

local r, ltn12 = pcall(require, 'ltn12')        -- from LuaSocket
if not r then
    diag "ltn12.source.file emulated"
    ltn12 = { source = {} }

    function ltn12.source.file (handle)
        if handle then
            return function ()
                local chunk = handle:read(1)
                if not chunk then
                    handle:close()
                end
                return chunk
            end
        else return function ()
                return nil, "unable to open file"
            end
        end
    end
end

local u = require 'ubjson'

local vectors = {
    { nil,          '5A',                                                   '[Z]',                                      "null" },
    { nil,          '4E',                                                   '[N]',                                      "no-op" },
    { true,         '54',                                                   '[T]',                                      "true" },
    { false,        '46',                                                   '[F]',                                      "false" },
    { 0,            '55 00',                                                '[U][U:0]',                                 "0 uint8" },
    { 0,            '69 00',                                                '[i][i:0]',                                 "0 int8" },
    { 0,            '49 00 00',                                             '[I][I:0]',                                 "0 int16" },
    { 0,            '6C 00 00 00 00',                                       '[l][l:0]',                                 "0 int32" },
    { 0,            '4C 00 00 00 00 00 00 00 00',                           '[L][L:0]',                                 "0 int64" },
    { 255,          '55 FF',                                                '[U][U:255]',                               "255 uint8" },
    { -1,           '69 FF',                                                '[i][i:-1]',                                "-1 int8" },
    { -1,           '49 FF FF',                                             '[I][I:-1]',                                "-1 int16" },
    { -1,           '6C FF FF FF FF',                                       '[l][l:-1]',                                "-1 int32" },
    { -1,           '4C FF FF FF FF FF FF FF FF',                           '[L][L:-1]',                                "-1 int64" },
    { 63,           '69 3F',                                                '[i][i:63]',                                "63 int8" },
    { 4095,         '49 0F FF',                                             '[I][I:4095]',                              "4095 int16" },
    { 16777215,     '6C 00 FF FF FF',                                       '[l][l:16777215]',                          "16777215 int32" },
    { 68719476735,  '4C 00 00 00 0F FF FF FF FF',                           '[L][L:68719476735]',                       "68719476735 int64" },
    { 72057594037927935,    '4C 00 FF FF FF FF FF FF FF',                   '[L][L:72057594037927935]',                 "72057594037927935 int64" },
    { -64,          '69 C0',                                                '[i][i:-64]',                               "-64 int8" },
    { -2048,        '49 F8 00',                                             '[I][I:-2048]',                             "-2048 int16" },
    { -8388608,     '6C FF 80 00 00',                                       '[l][l:-8388608]',                          "-8388608 int32" },
    { -34359738368, '4C FF FF FF F8 00 00 00 00',                           '[L][L:-34359738368]',                      "-34359738368 int64" },
    { -36028797018963968,   '4C FF 80 00 00 00 00 00 00',                   '[L][L:-36028797018963968]',                "-36028797018963968 int64" },
    { 0.0,          '64 00 00 00 00',                                       '[d][d:0.0]',                               "0.0 float32" },
    { 0.0,          '44 80 00 00 00 00 00 00 00',                           '[D][D:0.0]',                               "0.0 float64" },
    { '0.0',        '48 55 03 30 2E 30',                                    '[H][U][U:3][H:0.0]',                       "0.0 high-precision" },
    { -0.0,         '64 00 00 00 00',                                       '[d][d:0.0]',                               "-0.0 float32" },
    { -0.0,         '44 80 00 00 00 00 00 00 00',                           '[D][D:0.0]',                               "-0.0 float64" },
    { '-0.0',       '48 55 04 2D 30 2E 30',                                 '[H][U][U:4][H:-0.0]',                      "-0.0 high-precision" },
    { 'a',          '43 61',                                                '[C][C:a]',                                 "char 'a'" },
    { 'abc',        '53 55 03 61 62 63',                                    '[S][U][U:3][S:abc]',                       "'abc' string #uint8" },
    { 'abc',        '53 69 03 61 62 63',                                    '[S][i][i:3][S:abc]',                       "'abc' string #int8" },
    { 'abc',        '53 49 00 03 61 62 63',                                 '[S][I][I:3][S:abc]',                       "'abc' string #int16" },
    { 'abc',        '53 6C 00 00 00 03 61 62 63',                           '[S][l][l:3][S:abc]',                       "'abc' string #int32" },
    { 'abc',        '53 4C 00 00 00 00 00 00 00 03 61 62 63',               '[S][L][L:3][S:abc]',                       "'abc' string #int64" },
    { '',           '53 55 00',                                             '[S][U][U:1][S:a]',                         "'' string #uint8" },
    { '',           '53 69 00',                                             '[S][i][i:1][S:a]',                         "'' string #int8" },
    { '',           '53 49 00 00',                                          '[S][I][I:1][S:a]',                         "'' string #int16" },
    { '',           '53 6C 00 00 00 00',                                    '[S][l][l:1][S:a]',                         "'' string #int32" },
    { '',           '53 4C 00 00 00 00 00 00 00 00',                        '[S][L][L:1][S:a]',                         "'' string #int64" },
    { { 'a' },      '5B 43 61 5D',                                          '[\\[][C][C:a][\\]]',                       "['a'] array #indefinite" },
    { { 'a' },      '5B 23 55 01 43 61',                                    '[\\[][#][U][U:1][C][C:a]',                 "['a'] array #uint8" },
    { { 'a' },      '5B 23 69 01 43 61',                                    '[\\[][#][i][i:1][C][C:a]',                 "['a'] array #int8" },
    { { 'a' },      '5B 23 49 00 01 43 61',                                 '[\\[][#][I][I:1][C][C:a]',                 "['a'] array #int16" },
    { { 'a' },      '5B 23 6C 00 00 00 01 43 61',                           '[\\[][#][l][l:1][C][C:a]',                 "['a'] array #int32" },
    { { 'a' },      '5B 23 4C 00 00 00 00 00 00 00 01 43 61',               '[\\[][#][L][L:1][C][C:a]',                 "['a'] array #int64" },
    { 'ab',         '5B 24 55 23 55 02 61 62',                              '[\\[][$][U][#][U][U:2][U:97][U:98]',       "'ab' binary #uint8" },
    { 'ab',         '5B 24 55 23 69 02 61 62',                              '[\\[][$][U][#][i][i:2][U:97][U:98]',       "'ab' binary #int8" },
    { 'ab',         '5B 24 55 23 49 00 02 61 62',                           '[\\[][$][U][#][I][I:2][U:97][U:98]',       "'ab' binary #int16" },
    { 'ab',         '5B 24 55 23 6C 00 00 00 02 61 62',                     '[\\[][$][U][#][l][l:2][U:97][U:98]',       "'ab' binary #int32" },
    { 'ab',         '5B 24 55 23 4C 00 00 00 00 00 00 00 02 61 62',         '[\\[][$][U][#][L][L:2][U:97][U:98]',       "'ab' binary #int64" },
    { { true, true, true }, '5B 24 54 23 55 03',                            '[\\[][$][T][#][U][U:3]',                   "typed array #uint8" },
    { { true, true, true }, '5B 24 54 23 69 03',                            '[\\[][$][T][#][i][i:3]',                   "typed array #int8" },
    { { true, true, true }, '5B 24 54 23 49 00 03',                         '[\\[][$][T][#][I][I:3]',                   "typed array #int16" },
    { { true, true, true }, '5B 24 54 23 6C 00 00 00 03',                   '[\\[][$][T][#][l][l:3]',                   "typed array #int32" },
    { { true, true, true }, '5B 24 54 23 4C 00 00 00 00 00 00 00 03',       '[\\[][$][T][#][L][L:3]',                   "typed array #int64" },
    { {{1, 2}, {3, 4}},     '5B 5B 55 01 55 02 5D 5B 55 03 55 04 5D 5D',    '[\\[][\\[][U][U:1][U][U:2][\\]][\\[][U][U:3][U][U:4][\\]][\\]]',   "array of array" },
    { { A = 'a' },  '7B 55 01 41 43 61 7D',                                 '[{][U][U:1][S:A][C][C:a][}]',              "{\"A\":\"a\"} object #indefinite" },
    { { A = 'a' },  '7B 23 55 01 55 01 41 43 61',                           '[{][#][U][U:1][U][U:1][S:A][C][C:a]',      "{\"A\":\"a\"} object #uint8" },
    { { A = 'a' },  '7B 23 69 01 55 01 41 43 61',                           '[{][#][i][i:1][U][U:1][S:A][C][C:a]',      "{\"A\":\"a\"} object #int8" },
    { { A = 'a' },  '7B 23 49 00 01 55 01 41 43 61',                        '[{][#][I][I:1][U][U:1][S:A][C][C:a]',      "{\"A\":\"a\"} object #int16" },
    { { A = 'a' },  '7B 23 6C 00 00 00 01 55 01 41 43 61',                  '[{][#][l][l:1][U][U:1][S:A][C][C:a]',      "{\"A\":\"a\"} object #int32" },
    { { A = 'a' },  '7B 23 4C 00 00 00 00 00 00 00 01 55 01 41 43 61',      '[{][#][L][L:1][U][U:1][S:A][C][C:a]',      "{\"A\":\"a\"} object #int64" },
    { { A = 'a' },  '7B 24 43 23 55 01 55 01 41 61',                        '[{][$][C][#][U][U:1][U][U:1][S:A][C:a]',   "{\"A\":\"a\"} typed object #uint8" },
    { { A = 'a' },  '7B 24 43 23 69 01 55 01 41 61',                        '[{][$][C][#][i][i:1][U][U:1][S:A][C:a]',   "{\"A\":\"a\"} typed object #int8" },
    { { A = 'a' },  '7B 24 43 23 49 00 01 55 01 41 61',                     '[{][$][C][#][I][I:1][U][U:1][S:A][C:a]',   "{\"A\":\"a\"} typed object #int16" },
    { { A = 'a' },  '7B 24 43 23 6C 00 00 00 01 55 01 41 61',               '[{][$][C][#][l][l:1][U][U:1][S:A][C:a]',   "{\"A\":\"a\"} typed object #int32" },
    { { A = 'a' },  '7B 24 43 23 4C 00 00 00 00 00 00 00 01 55 01 41 61',   '[{][$][C][#][l][L:1][U][U:1][S:A][C:a]',   "{\"A\":\"a\"} typed object #int64" },
    { {ab = false}, '7B 24 46 23 55 01 55 02 61 62',                        '[{][$][F][#][U][U:1][U][U:2][S:ab]',       "{\"ab\":false} typed object #uint8" },
    { {ab = false}, '7B 24 46 23 69 01 55 02 61 62',                        '[{][$][F][#][i][i:1][U][U:2][S:ab]',       "{\"ab\":false} typed object #int8" },
    { {ab = false}, '7B 24 46 23 49 00 01 55 02 61 62',                     '[{][$][F][#][I][I:1][U][U:2][S:ab]',       "{\"ab\":false} typed object #int16" },
    { {ab = false}, '7B 24 46 23 6C 00 00 00 01 55 02 61 62',               '[{][$][F][#][l][l:1][U][U:2][S:ab]',       "{\"ab\":false} typed object #int32" },
    { {ab = false}, '7B 24 46 23 4C 00 00 00 00 00 00 00 01 55 02 61 62',   '[{][$][F][#][L][L:1][U][U:2][S:ab]',       "{\"ab\":false} typed object #int64" },
    { {A = {1, 2}}, '7B 55 01 41 5B 55 01 55 02 5D 7D',                     '[{][U][U:1][S:A][\\[][U][U:1][U][U:2][\\]][}]',    "object of array" },
    { {A={a='A'}},  '7B 55 01 41 7B 55 01 61 43 41 7D 7D',                  '[{][U][U:1][S:A][{][U][U:1][S:a][C][C:A][}][}]',   "object of object" },
}

local function unhex (s)
    local t = {}
    for v in s:gmatch'%x%x' do
        t[#t+1] = tonumber(v, 16)
    end
    return string.char(unpack(t))
end

local t = {}
for i = 1, #vectors do
    local data, ubj, _, comment = unpack(vectors[i])
    if #string.pack('n', 0.0) > 8 then
        data = tonumber(data) or data
    end
    ubj = unhex(ubj)
    if comment:match('64') and #string.pack('n', 0.0) == 4 then
        t[#t+1] = 'N'
    else
        t[#t+1] = ubj
    end
    vectors[i] = { data, ubj, _, comment }
end

local encoded = table.concat(t)
local f = io.open('stream.ubj', 'w')
f:write(encoded)
f:close()

local function exercise_decode_ref ()
    for i = 1, #vectors do
        local data, ubj, _, comment = unpack(vectors[i])
        if comment:match('64') and #string.pack('n', 0.0) == 4 then
            skip( "small Lua", 1 )
        else
            if type(data) == 'table' then
                same( u.decode(ubj), data, comment )
            else
                equals( u.decode(ubj), data, comment )
            end
        end
    end
end

local function exercise_decode_stream ()
    local i = 1
    f = io.open('stream.ubj', 'r')
    local s = ltn12.source.file(f)
    for _, val in u.decoder(s) do
        local data, _, _, comment = unpack(vectors[i])
        if comment:match('64') and #string.pack('n', 0.0) == 4 then
            skip( "small Lua", 1 )
        else
            if type(data) == 'table' then
                same( val, data, comment )
            else
                equals( val, data, comment )
            end
        end
        i = i + 1
    end
end

local function exercise_decode_encode ()
    for i = 1, #vectors do
        local data, _, _, comment = unpack(vectors[i])
        if comment:match('64') and string.pack('n', 0.0) == 4 then
            skip( "small Lua", 1 )
        else
            if type(data) == 'table' then
                same( u.decode(u.encode(data)), data, comment )
            else
                equals( u.decode(u.encode(data)), data, comment )
            end
        end
    end
end

plan((1 + 1 + 4) * #vectors)

exercise_decode_ref()

exercise_decode_stream()

diag("set_float'float32'")
u.set_float'float32'
exercise_decode_encode()

diag("set_float'float64'")
u.set_float'float64'
exercise_decode_encode()

diag("set_string'binary'")
u.set_string'binary'
exercise_decode_encode()

diag("set_string'string'")
u.set_string'text'
exercise_decode_encode()

os.remove 'stream.ubj'  -- clean up
