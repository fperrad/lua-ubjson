#! /usr/bin/lua

require'Test.Assertion'

plan(13)

local c1 = require'ubjson'
package.loaded['ubjson'] = nil     -- hack here
local c2 = require'ubjson'

not_equals( c1, c2 )

local t1 = { 10, 20, 30, 40, 50 }
equals( c1.encode(t1):byte(2), c1.open_array(5):byte(2), "sequence" )
same( c1.decode(c1.encode(t1)), t1 )
local t2 = { 10, 20, nil, 40 }
equals( c1.encode(t2):byte(2), c1.open_array(2):byte(2), "array truncated by hole" )
same( c1.decode(c1.encode(t2)), t2 )

c1.set_array'with_hole'
c2.set_array'without_hole'

equals( c1.encode(t1):byte(2), c1.open_array(5):byte(2), "sequence" )
same( c1.decode(c1.encode(t1)), t1 )
equals( c1.encode(t2):byte(2), c1.open_array(4):byte(2), "array with hole")
same( c1.decode(c1.encode(t2)), t2 )

equals( c2.encode(t1):byte(2), c2.open_array(5):byte(2), "sequence" )
same( c2.decode(c2.encode(t1)), t1 )
equals( c2.encode(t2):byte(2), c2.open_array(2):byte(2), "array truncated by hole" )
same( c2.decode(c2.encode(t2)), t2 )

