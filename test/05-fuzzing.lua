#! /usr/bin/lua

require'Test.Assertion'

local nb = tonumber(os.getenv('FUZZ_NB')) or 1000
local len = tonumber(os.getenv('FUZZ_LEN')) or 128

if nb <= 0 then
    skip_all('coverage')
else
    plan'no_plan'
    passes()
end

local u = require'ubjson'

local unpack = table.unpack or unpack
math.randomseed(os.time())
for _ = 1, nb do
    local t = {}
    for i = 1, len do
        t[i] = math.random(0, 255)
    end
    local data = string.char(unpack(t))
    local r, msg = pcall(u.decode, data)
    if r == true then
        passes()
    else
        if     not msg:match'extra bytes$'
           and not msg:match'missing bytes$'
           and not msg:match'negative size$'
           and not msg:match'type without size$'
           and not msg:match'duplicated keys$'
           and not msg:match'invalid UTF%-8 string$'
           and not msg:match'is unexpected$' then
            diag(table.concat(t, ' '))
            diag(msg)
            fails()
        end
    end
end

done_testing()
