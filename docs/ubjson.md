
# UBJSON

---

# Reference

#### encode( data )

Serialize a `data`.

#### decode( str )

Deserialize a `string`.

#### decoder( src )

Accept a `string` or a [`ltn12.source`](https://lunarmodules.github.io/luasocket/ltn12.html#source)
and returns a iterator.

The iterator gives a couple of values,
the _interesting_ value is the second one.

#### set_string( str )

Configures the behaviour of `encode`.
The valid options are `'binary'`, `'text'` and `'check_utf8'`.
The default is `'text'`.

#### set_array( str )

Configures the behaviour of `encode`.
The valid options are `'without_hole'`, `'with_hole'` and `'always_as_object'`.
The default is `'with_hole'`.

#### set_float( str )

Configures the behaviour of `encode`.
The valid options are `'float32'`, `'float64'` and `'high-precision'`.
The default is `'float64'` with a _standard_ Lua interpreter.

#### encode_name( str )

Encodes a _name_ or a _key_, ie. a string without the marker `S`
(typically used for streaming of object).

#### open_array( [count [, type]] )

Encodes the start of an array with by an optional count and an optional type marker.

#### open_object( [count [, type]] )

Encodes the start of an object with by an optional count and an optional type marker.

#### close_array()

Returns the marker `]`.

#### close_object()

Returns the marker `}`.

#### no_op()

Returns the marker `N`.

## Data Conversion

- The following __Lua__ types could be converted :
  `nil`, `boolean`, `number`, `string` and `table`.
- A __Lua__ `number` has one of two subtypes: `integer` and `float`.
  A __Lua__ `integer` is converted into an __UBJSON__ `integer`.
  A __Lua__ `float` is converted into the __UBJSON__ `float32` or `float64` or `high-precision` (see `set_float`).
- When a __Lua__ `table` is converted into __UBJSON__, some parts could be discarded
  (like __JSON__, the only permitted _key_ or _name_ of an __UBJSON__ `object` are `string`).
- A __Lua__ `table` is converted into a __UBJSON__ `array`
  only if _all_ the keys are composed of strictly positive integers,
  without hole or with holes (see `set_array`).
  Otherwise it is converted into __UBJSON__ `object`.
- With `set_array'without_hole'`, all values after a hole are discarded.
- With `set_array'always_as_object'`,
  all __Lua__ `table` are converted into a __UBJSON__ `object` (non-string keys are discarded).
- LIMITATION : __UBJSON__ cannot handle data with _cyclic_ reference.

# Examples

## Basic usage

```lua
local u = require'ubjson'

u.set_float'float32'
u.set_array'with_hole'
u.set_string'text'

ubj = u.encode(data)
data = u.decode(ubj)
```

See other usages in [Stream](stream.md).

## Advanced usage

The following Lua hack allows to have several instances
of the module `ubjson`, each one with its own settings.

```lua
local u1 = require'ubjson'
package.loaded['ubjson'] = nil    -- the hack is here
local u2 = require'ubjson'

u1.set_array'without_hole'
u2.set_array'always_as_object'
```
