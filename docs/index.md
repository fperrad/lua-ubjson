
# lua-ubjson

---

## Overview

The [UBJSON](https://ubjson.org/) (Universal Binary JSON) is a data interchange format.
It is a binary form directly imitating JSON, but requiring fewer bytes of data.

lua-ubjson is a pure Lua implementation (with an only dependency on
[compat53](https://github.com/lunarmodules/lua-compat-5.3/)
with Lua 5.1 & 5.2)
and aligned with the
[Draft 12](https://github.com/ubjson/universal-binary-json/tree/master/spec12)
specifications.

## References

The homepage of UBJSON is available at <https://ubjson.org/>.

## Status

lua-ubjson is in alpha stage.

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.


## Download

The sources are hosted on [Framagit](https://framagit.org/fperrad/lua-ubjson).

The Teal type definition of this library is available
[here](https://github.com/teal-language/teal-types/blob/master/types/lua-ubjson/ubjson.d.tl).

## Installation

lua-ubjson is available via LuaRocks:

```sh
luarocks install lua-ubjson
#luarocks install compat53
```

or manually, with:

```sh
make install LUAVER=5.3
```

## Test

The test suite requires the module
[lua-TestAssertion](https://fperrad.frama.io/lua-testassertion/).

```sh
make test
```

## Copyright and License

Copyright &copy; 2023 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
