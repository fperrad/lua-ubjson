
# Stream

---

## Emitter

### Indefinite-Length Array

```lua
local u = require'ubjson'

s:write(u.open_array())
...
s:write(u.encode(element))
...
s:write(u.close_array())
```

### Indefinite-Length Map

```lua
local u = require'ubjson'

s:write(u.open_object())
...
s:write(u.encode_name(key))
s:write(u.encode(value))
...
s:write(u.open_object())
```

### No-Op

```lua

...
s:write(u.no_op())
...
```

## Receiver

The function `decoder` accepts a _compatible_
[`ltn12.source`](https://lunarmodules.github.io/luasocket/ltn12.html#source)
source.

A source is a function that produces data, chunk by chunk.
See <http://lua-users.org/wiki/FiltersSourcesAndSinks>.

```lua
local u = require'ubjson'
local ltn12 = require'ltn12'
src = ltn12.source.file(io.open('file', 'r'))
for _, v in u.decoder(src) do
    print(v)
end

-- when only one value
local _, v = u.decoder(src)()
```
