lua-ubjson : UBJSON
===================

Introduction
------------

The [UBJSON](https://ubjson.org/) (Universal Binary JSON) is a data interchange format.
It is a binary form directly imitating JSON, but requiring fewer bytes of data.

lua-ubjson is a pure Lua implementation (with an only dependency on
[compat53](https://github.com/lunarmodules/lua-compat-5.3/)
with Lua 5.1 & 5.2)
and aligned with the
[Draft 12](https://github.com/ubjson/universal-binary-json/tree/master/spec12)
specifications.

Links
-----

The homepage is at <https://fperrad.frama.io/lua-ubjson/>,
and the sources are hosted at <https://framagit.org/fperrad/lua-ubjson/>.

Copyright and License
---------------------

Copyright (c) 2023 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.
