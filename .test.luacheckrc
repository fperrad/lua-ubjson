read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'subtest',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Assertion
    'equals',
    'not_equals',
    'array_equals',
    'same',
    'falsy',
    'truthy',
    'is_function',
    'is_number',
    'is_string',
    'is_table',
    'matches',
    'fails',
    'passes',
    'error_matches',
    'not_errors',
    'require_ok',
}

files['test/01-spectest.lua'].max_line_length = 200
